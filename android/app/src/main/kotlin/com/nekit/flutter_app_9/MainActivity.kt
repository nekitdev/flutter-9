package com.nekit.flutter_app_9

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        resultFromNative()
    }

    @SuppressLint("HardwareIds")
    private fun resultFromNative() {
        MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == KEY_NATIVE) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
                    val id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
                    result.success(id)
                }
            } else {
                result.notImplemented()
            }
        }
    }

    companion object {
        const val CHANNEL = "com.nekit.flutter_app_9.native_channel"
        const val KEY_NATIVE = "showIdFromNative"
    }
}
