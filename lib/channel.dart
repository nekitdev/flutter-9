import 'package:flutter/services.dart';

const CHANNEL = "com.nekit.flutter_app_9.native_channel";

class NativeChannel {
  static const platform = const MethodChannel(CHANNEL);
}
