import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_9/channel.dart';

void main() => runApp(MyApp());

const KEY_NATIVE = "showIdFromNative";

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter 9'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  static const platform = const MethodChannel(CHANNEL);
  String _responseFromNative = "";

  Future<void> _showToastFromNative() async {
    String result = await platform.invokeMethod(KEY_NATIVE);
    setState((){
      _responseFromNative = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton (
              child: Text('Get data from native'),
              onPressed: _showToastFromNative,
            ),
            Text(_responseFromNative)
          ],
        ),
      ),
    );
  }
}
